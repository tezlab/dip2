<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_task".
 *
 * @property integer $id
 * @property integer $task_id
 * @property string $code
 * @property string $is_correct
 *
 * @property Task $task
 */
class UserTask extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id'], 'required'],
            [['task_id'], 'integer'],
            [['code', 'is_correct'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'Task ID',
            'code' => 'Code',
            'is_correct' => 'Is Correct',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }
    public function getUser()
    {
        return $this->hasOne(\webvimark\modules\UserManagement\models\User::className(), ['id' => 'user_id']);
    }
    public static function getUserTasks($user_id, $is_correct = NULL){
        if($is_correct!==NULL)
            return count(UserTask::findAll(['user_id' => $user_id, 'is_correct' => (string)$is_correct]));
        else
            return count(UserTask::findAll(['user_id' => $user_id]));
    }
}
