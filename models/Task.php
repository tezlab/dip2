<?php

namespace app\models;

use Yii;
require_once dirname(__DIR__).'/parser.php';

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $begin
 * @property string $body
 * @property string $end
 * @property string $result
 * @property string $extra
 * @property integer $category_id
 * @property integer $checktype
 *
 * @property Category $category
 * @property UserTask[] $userTasks
 */
class Task extends \yii\db\ActiveRecord
{
    
    const ITERATION_COUNT = 20;
    const EXECUTION_TIME = 15;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id'], 'required'],
            [['description', 'begin', 'body', 'end', 'result', 'extra'], 'string'],
            [['category_id', 'checktype'], 'integer'],
            [['name', 'result'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название задачи',
            'description' => 'Описание задачи',
            'begin' => 'Начало программы',
            'body' => 'Тело программы',
            'end' => 'Конец программы',
            'result' => 'Ответ',
            'extra' => 'Extra',
            'category_id' => 'Категория',
            'checktype' => 'Тип проверки'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTasks()
    {
        return $this->hasMany(UserTask::className(), ['task_id' => 'id']);
    }
    
    //Запуск программы на компиляторе
    static function run($code){
        $filename = Yii::$app->basePath . '/runtime/' . date('Ymd_H_i_s');
        $res_file = Yii::$app->basePath . '/runtime/info.txt';
        $compile = '';
        $result = '';
        $compile_res = -1;
        if (file_exists($res_file)) {
            file_put_contents($res_file, '');
            if (file_put_contents($filename . '.c', $code)) {

                $descriptorspec = array(
                    0 => array("pipe", "r"), // stdin - канал, из которого дочерний процесс будет читать
                    1 => array("pipe", "w"), // stdout - канал, в который дочерний процесс будет записывать 
                    2 => array("file", $res_file, "a") // stderr - файл для записи
                );
                $process = proc_open("gcc $filename.c -o $filename", $descriptorspec, $pipes);

                if (is_resource($process)) { 
                    $compile_res = proc_close($process);
                    $compile = file_get_contents($res_file); 
                    //сохраняем результат в отдельном файле                    
                    //file_put_contents($filename . '_info.txt', $compile);
                }
                if ($compile_res == 0)
                    $result = shell_exec('timeout '.Task::EXECUTION_TIME.' '.$filename);
                
                //Удаляем временные файлы
                if(file_exists($filename))
                    unlink ($filename);
                if(file_exists($filename.'.c'))
                    unlink ($filename.'.c');
                if(file_exists($filename.'_info.txt'))
                    unlink ($filename. '_info.txt');                
            } else
                $result = "error";
        } else
            Yii::$app->session->setFlash('danger', "<strong>Компиляция не возможна.</strong> Файл $res_file не существует.");
        return ['compile' => $compile, 'compile_res' => $compile_res, 'result' => $result];
    }
    
    //Перебирает переменные
    public function check($code = null){
        if($code == null){
            $code = $this->begin.$this->body.$this->end;
            $origin = true;            
        }
        else
            $origin = false;
        
        $vars = json_decode($this->extra, true);
        $result = [];
        for($iter=0; $iter<Task::ITERATION_COUNT; $iter++){
            $iter_res = [];
            $iter_res['input'] = '';
            if(!$origin)
                $origin_code = $this->begin.$this->body.$this->end;
            foreach($vars['varname'] as $i => $var){
                $params = explode(";", $vars['varparam'][$i]);
                $val = rand($params[0],$params[1]);
                $var = trim($var);  
                $iter_res['input'] = $iter_res['input'] . $var.'='.$val.'; ';
                $code = preg_replace("/$var\s*=\s*-*\s*\d+;/", "$var=$val;", $code);     
                if(!$origin)
                    $origin_code = preg_replace("/$var\s*=\s*-*\s*\d+;/", "$var=$val;", $origin_code);  
            }
            $iter_res['code'] = $code;
            if(!$origin)
                $iter_res['origin_out'] = Task::run($origin_code);
            $iter_res['output'] = Task::run($code);
            if(!$origin){
                $iter_res['decided'] = false;
                if(strpos($iter_res['output']['result'], $iter_res['origin_out']['result'])!==false)
                    $iter_res['decided'] = true;
            }
            $result[] = $iter_res;
        }
        return $result;
        
    }
    public function beforeSave($insert) {
        if(isset($_POST['varname'])){
            $vars = [
                'varname' => $_POST['varname'],
                'vartype' => $_POST['vartype'],
                'varparam' => $_POST['varparam'],
            ];
            $this->extra = json_encode($vars);
        }
        return parent::beforeSave($insert);
    }
    public function getChecktypes(){
        return [
            0 => 'Проверка с ответом',
            1 => 'Перебор переменных и сравнение',
        ];
    }
    public static function getTree($body){
        $l = new \Lexer(); 
        $l->code = $body;        
        $tokens = $l->getTokens();
        $p = new \Parser();
        $p->tokens = $tokens;
        return $p->parse();        
    }
    public function getUserTask(){
        return UserTask::findOne(['task_id' => $this->id, 'user_id' => Yii::$app->user->id]);
    }
}
