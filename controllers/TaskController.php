<?php

namespace app\controllers;

use Yii;
use app\models\Task;
use app\models\UserTask;
use app\models\TaskSearch;
use webvimark\modules\UserManagement\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller {

    public $defaultAction = 'list';
    public $studentActions = ['list', 'decide'];
    public $adminActions = ['index', 'test', 'view', 'check', 'create', 'delete', 'update', 'saveresult', 'student', 'students', 'test-student-task'];

    public function behaviors() {
        return [
//            'ghost-access' => [
//                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
//            ],
            'verbs'        => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel'  => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Task();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $model->begin = "#include <stdlib.h>
#include <stdio.h>
int main(){";
            $model->end = "return 0;
}";
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionList() {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
                    'searchModel'  => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTest($id) {
        $model = $this->findModel($id);
        $code = $model->begin . $model->body . $model->end;
        $res = Task::run($code);
        return $this->render('test', \yii\helpers\ArrayHelper::merge(['model' => $model], $res));
    }

    public function actionTree($id) {
        $model = $this->findModel($id);
        return $this->render('tree', ['tree' => Task::getTree($model->body)]);
    }

    public function actionSaveresult($id) {
        $model = $this->findModel($id);
        $code = $model->begin . $model->body . $model->end;
        $res = Task::run($code);
        if (isset($res['result'])) {
            $model->result = $res['result'];
            if ($model->save())
                Yii::$app->session->setFlash('success', "Ответ сохранен.");
        } else
            Yii::$app->session->setFlash('warning', "Ошибка сохранения.");
        return $this->redirect(['view', 'id' => $model->id]); //$this->render('test', \yii\helpers\ArrayHelper::merge(['model' => $model], $res));            
    }

    public function actionDecide($id) {
        $model = $this->findModel($id);


        $user_task = UserTask::findOne(['task_id' => $id, 'user_id' => Yii::$app->user->id]);
        if (!isset($user_task)) {
            $user_task = new UserTask();
            $user_task->task_id = $id;
            $user_task->user_id = Yii::$app->user->id;
        }
        if (isset($_POST['code'])) {
            $user_task->code = $_POST['code'];
            $res = Task::run($model->begin . $_POST['code'] . $model->end);
            if ($res['result'] && $res['compile_res'] == 0) {
                if ($model->checktype == 0) {
                    if ($model->result)
                        if (strpos(trim($res['result']), $model->result) !== false) {
                            Yii::$app->session->setFlash('success', "Задача решена правильно.");
                            $user_task->is_correct = '1';
                        } else {
                            Yii::$app->session->setFlash('warning', 'Задача решена не верно.');
                            $user_task->is_correct = '0';
                        }
                    $user_task->save();
                    return $this->render('decide', ['model' => $model, 'user_task' => $user_task, 'res' => $res]);
                } else {
                    $res2 = $model->check($model->begin . $_POST['code'] . $model->end);
                    $true_count = 0;
                    foreach ($res2 as $item)
                        if ($item['decided'])
                            $true_count++;
                    if ($true_count == count($res2)) {
                        Yii::$app->session->setFlash('success', 'Задача решена правильно.');
                        $user_task->is_correct = '1';
                    } else {
                        Yii::$app->session->setFlash('warning', 'Задача решена не верно.');
                        $user_task->is_correct = '0';
                    }
                    $user_task->save();
                    return $this->render('decide', ['model' => $model, 'user_task' => $user_task, 'res' => $res, 'res2_all' => $res2]);
                }
            } else {
                $user_task->is_correct = '0';
                $user_task->save();
                Yii::$app->session->setFlash('danger', 'Ошибка компиляции.');
                return $this->render('decide', ['model' => $model, 'user_task' => $user_task, 'res' => $res]);
            }
        } else
            return $this->render('decide', ['model' => $model, 'user_task' => $user_task]);
    }

    public function actionCheck($id) {
        $model = $this->findModel($id);
        $res = $model->check();
        return $this->render('view', ['model' => $model, 'res' => $res]);
    }

    public function actionStudents() {
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \webvimark\modules\UserManagement\models\User::find()->joinWith(['roles'])->where([Yii::$app->getModule('user-management')->auth_item_table . '.name' => 'student']),
        ]);
        return $this->render('students', ['dataProvider' => $dataProvider]);
    }

    public function actionStudent($id) {
        $models = UserTask::findAll(['user_id' => $id]);
        return $this->render('student', ['model' => User::findOne($id), 'tasks' => $models]);
    }

    public function actionTestStudentTask($id, $student_id) {
        $model = $this->findModel($id);
        $student = User::findOne($student_id);

        $user_task = UserTask::findOne(['task_id' => $id, 'user_id' => $student_id]);

        if (isset($_POST['code'])) {
            //$user_task->code = $_POST['code'];
            $res = Task::run($model->begin . $_POST['code'] . $model->end);
            if ($res['result'] && $res['compile_res'] == 0) {
                if ($model->checktype == 0) {
                    if ($model->result)
                        if (strpos(trim($res['result']), $model->result) !== false) {
                            Yii::$app->session->setFlash('success', "Задача решена правильно.");
                            //$user_task->is_correct = '1';
                        } else {
                            Yii::$app->session->setFlash('warning', 'Задача решена не верно.');
                            //$user_task->is_correct = '0';
                        }
                    //$user_task->save();
                    return $this->render('decide', ['model' => $model, 'user_task' => $user_task, 'res' => $res]);
                } else {
                    $res2 = $model->check($model->begin . $_POST['code'] . $model->end);
                    $true_count = 0;
                    foreach ($res2 as $item)
                        if ($item['decided'])
                            $true_count++;
                    if ($true_count == count($res2)) {
                        Yii::$app->session->setFlash('success', 'Задача решена правильно.');
                        //$user_task->is_correct = '1';
                    } else {
                        Yii::$app->session->setFlash('warning', 'Задача решена не верно.');
                        //$user_task->is_correct = '0';
                    }
                    //$user_task->save();
                    return $this->render('decide', ['model' => $model, 'user_task' => $user_task, 'res' => $res, 'res2_all' => $res2]);
                }
            } else {
                //$user_task->is_correct = '0';
                //$user_task->save();
                Yii::$app->session->setFlash('danger', 'Ошибка компиляции.');
                return $this->render('decide', ['model' => $model, 'user_task' => $user_task, 'res' => $res]);
            }
        } else
            return $this->render('decide', ['model' => $model, 'user_task' => $user_task]);
    }

    public function beforeAction($action) {
        if (parent::beforeAction($action)) {
            if(User::hasRole(['student'], false)){
                if(in_array($action->id, $this->studentActions))
                        return true;
            }
            elseif(User::hasRole(['Admin'])){
                if(in_array($action->id, $this->adminActions))
                        return true;
            }
            elseif(Yii::$app->user->isGuest)
                $this->redirect (['/']);
        }
        throw new NotFoundHttpException('The requested page does not exist.');
        return false;
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
