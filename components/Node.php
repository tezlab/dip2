<?php
namespace app\components;

class Node {
    public $kind;
    public $value;
    public $op1;
    public $op2;
    public $op3;
    function Node($kind, $value = null, $op1 = null, $op2 = null, $op3 = null){
        $this->kind = $kind;
        $this->value = $value;
        $this->op1 = $op1;
        $this->op2 = $op2;
        $this->op3 = $op3;
    }
}