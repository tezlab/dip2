<?php
namespace app\components;

class Parser 
{
    public $operations = ['+', '-', '*', '/'];
    public $debug = false;
    public $tokens = [];
    public $token;
    public $i = 0;
    public function next_tok(){
        if($this->debug)
            echo "token_id = $this->i\n";
        if(isset($this->tokens[$this->i])){
            $this->token =  $this->tokens[$this->i];
            //print_r($this->token);
            $this->i++;
        }
        else
            $this->token = 'EOF';
    }
    public function term(){
        if($this->debug)
            echo "term \n";
        if($this->token['sym'] == 'var'){
            $n = new Node('var', $this->token['value']);
            $this->next_tok();
            return $n;
        }
        elseif($this->token['sym'] == 'num'){
            $n = new Node('num', $this->token['value']);
            echo $this->token['value']."\n";
            $this->next_tok();
            echo $this->token['value']."\n";
            return $n;
        }
        else
            return $this->paren_expr ();
    }
    public function summa(){
        if($this->debug)
            echo "summa \n";
        $n = $this->term();
        while(in_array($this->token['value'], $this->operations)){
            switch ($this->token['value']){
                case '+': $kind = 'add'; break;
                case '-': $kind = 'sub'; break;
                case '*': $kind = 'mul'; break;
                case '/': $kind = 'div'; break;
            }            
            $this->next_tok();
            $n = new Node($kind, $op1 = $n, $op2 = $this->term());
        }
        return $n;
    }
    public function test(){
        if($this->debug)
            echo "test \n";
        $n = $this->summa();
        switch($this->token['value']){
            case '<':
                $this->next_tok();
                $n = new Node('lt', $op1 = $n, $op2 = $this->summa());
                break;
            case '>':
                $this->next_tok();
                $n = new Node('mt', $op1 = $n, $op2 = $this->summa());
                break;
            case '==':
                $this->next_tok();
                $n = new Node('eq', $op1 = $n, $op2 = $this->summa());
                break;
            case '<=':
                $this->next_tok();
                $n = new Node('le', $op1 = $n, $op2 = $this->summa());
                break;
            case '>=':
                $this->next_tok();
                $n = new Node('me', $op1 = $n, $op2 = $this->summa());
                break;
            default: break;
                
        }
        return $n;
    }
    public function expr(){
        if($this->debug)
            echo "expr \n";
        if($this->token['sym'] != 'var')
            return $this->test();
        $n = $this->test();
        if($n->kind == 'var' && $this->token['value'] == '='){
            $this->next_tok();
            $n = new Node('set', $op1 = $n, $op2 = $this->expr());
        }
        return $n;
            
    }
    public function paren_for_expr(){
        if($this->debug)
            echo "paren_for_expr \n";
        if($this->token['value'] != '(')
            die("'(' expexted in paren for expr\n");
        $this->next_tok();
        $n1 = $this->expr();
        if($this->token['value'] != ';')
            die("';' expected in paren for expr\n");
        $this->next_tok();
        $n2 = $this->test();
        if($this->token['value'] != ';')
            die("';' expected in paren for expr\n");
        $this->next_tok();
        $n3 = $this->expr();
        if($this->token['value'] != ')')
            die("')' expected in paren for expr\n");
        $this->next_tok();
        $n = new Node('forp', $op1 = $n1, $op2 = $n2, $op3 = $n3); 
        return $n;
    }
    public function paren_expr(){
        if($this->debug)
            echo "paren_expr \n";
        if($this->token['value'] != '(')
            die("'(' expexted \n");
        $this->next_tok();
        $n = $this->expr();
        if($this->token['value'] != ')')
            echo "')' expected \n";
        $this->next_tok();
        return $n;        
    }
    public function statement(){
        if($this->debug)
            echo "statement \n";
        if($this->token['value'] == 'if'){
            $n = new Node('if1');
            $this->next_tok();
            $n->op1 = $this->paren_expr();
            $n->op2 = $this->statement();
            if($this->token['value'] == 'else'){
                $n->kind = 'if2';
                $this->next_tok();
                $n->op3 = $this->statement();
                
            }
        }
        elseif($this->token['value'] == 'for'){
            $n = new Node('for');
            $this->next_tok();
            $n->op1 = $this->paren_for_expr();
            $n->op2 = $this->statement();
        }
        elseif($this->token['value'] == 'while'){
            $n = new Node('while');
            $this->next_tok();
            $n->op1 = $this->paren_expr();
            $n->op2 = $this->statement();
        }
        elseif($this->token['value'] == 'do'){
            $n = new Node('do');
            $this->next_tok();
            $n->op1 = $this->statement();
            if($this->token['value'] != 'while')
                echo "'while' expected \n";
            $this->next_tok();
            $n->op2 = $this->paren_expr();
            if($this->token['value'] != ';')
                echo "';' expected \n";
        }
        elseif($this->token['value'] == ';'){
            $n = new Node('emp');
            $this->next_tok();
        }
        elseif($this->token['value'] == '{'){
            $n = new Node('emp');
            $this->next_tok();
            while($this->token['value'] != '}')
                $n = new Node('seq', $op1 = $n, $op2 = $this->statement ());
            $this->next_tok();
        }
        else{
            $n = new Node('expr', $op1 = $this->expr());
            if($this->token['value'] != ';'){
                //var_dump($n);
                die("';' expected on '{$this->token['value']}' token_num = {$this->i}\n");
            }
            $this->next_tok();
        }
        return $n;
            
    }

    public function parse(){
        $this->next_tok();
        $node = new Node('prog', $op1 = $this->statement());
        if($this->token != 'EOF')
            echo "Invalid statement sintax\n";
        return $node;
    }
    
    
}



//$l = new Lexer();
//$l->debug = true;
//$l->code = '{a=1;
//b=1000;
//for(a=1;a<b;a=a+1)
//s=a+b;}';
//$p = new Parser();
//$tokens = $l->getTokens();
//
////print_r($tokens);
//$p->debug = true;
//$p->tokens = $tokens;
//$tree = $p->parse();


//print_r($tree);