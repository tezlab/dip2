<?php
namespace app\components;

class Lexer {
    public $code = '';
    public $symbols = [';', '{', '}', '=', '(', ')', '+', '-', '*', '/', '<', '>', '<=', '>=', '==', '++', '--', '+=', '-=', '*=', '/='];
    public $words = ['if', 'else', 'for', 'while', 'do', 'break'];
    public $debug = false;
    protected function is_alpha($a){
        if(ord($a) >= ord('a') && ord($a) <= ord('z'))
            return true;
        else
            false;
    }
    public function getTokens(){
        $this->code = str_ireplace(["\n", " ", "\t"] , "", $this->code);
        //$this->code = str_replace(" ", "", $this->code);
        $_code = str_split($this->code);
        if($this->debug)
            print_r($_code);
        $tokens = [];        
        for($i=0; $i<count($_code); $i++){
            $_code[$i] = trim($_code[$i]);
            if($this->debug)
                echo $_code[$i]. ' - ' . $i. '/'. count($_code)." - ASCII = ".ord($_code[$i])."\n";            
            $token = [];            
            if($_code[$i] === ' ' || ord($_code[$i]) == 0)
                continue;
            elseif (in_array($_code[$i], $this->symbols)) {
                $token['sym'] = 'sym';                
                if(isset($_code[$i+1]) && $_code[$i+1] == '=')
                    $token['value'] = $_code[$i].'=';        
                elseif($_code[$i] == '+' && $_code[$i+1] == '+')
                    $token['value'] = '++';
                elseif($_code[$i] == '-' && $_code[$i+1] == '-')
                    $token['value'] = '--';
                else
                    $token['value'] = $_code[$i];
                $tokens[] = $token;
            }
            elseif (is_numeric($_code[$i])){
                $intval = 0;
                $floatval = 0;         
                while(is_numeric($_code[$i])){                    
                    $intval = $intval * 10 + (int)$_code[$i];
                    $i++;                    
                }
                if($_code[$i] == '.'){
                    $i++;
                    while(is_numeric($_code[$i])){                    
                        $floatval = $floatval * 10 + (int)$_code[$i];
                        $i++;                    
                    }
                    if($floatval > 0)
                        $intval += $floatval * pow (0.1, strlen ((string)$floatval));
                }
                $token['sym'] = 'num';
                $token['value'] = $intval;
                $tokens[] = $token;
                $i--;
            }
            elseif($this->is_alpha($_code[$i])){
                $ident = '';                
                while($this->is_alpha($_code[$i])){
                    $ident .= $_code[$i];
                    $i++;
                }
                $i--;
                if(in_array($ident, $this->words)){
                    $token['sym'] = 'words';
                    $token['value'] = $ident;
                    $tokens[] = $token;
                }
                else{
                    $token['sym'] = 'var';
                    $token['value'] = $ident;
                    $tokens[] = $token;
                }
                
            }
            else
                echo "Unexpected symbol: '$_code[$i]'";
        }
        return $tokens;
    }
}