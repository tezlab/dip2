<?php

class Lexer {
    public $code = '';
    public $symbols = [';', '{', '}', '=', '(', ')', '+', '-', '*', '/', '<', '>', '<=', '>=', '==', '++', '--', '+=', '-=', '*=', '/='];
    public $words = ['if', 'else', 'for', 'while', 'do', 'break'];
    public $debug = false;
    protected function is_alpha($a){
        if(ord($a) >= ord('a') && ord($a) <= ord('z'))
            return true;
        else
            false;
    }
    public function getTokens(){
        $this->code = str_ireplace(["\n", " ", "\t"] , "", $this->code);
        //$this->code = str_replace(" ", "", $this->code);
        //echo $this->code;
        $_code = str_split($this->code);
        if($this->debug)
            print_r($_code);
        $tokens = [];        
        for($i=0; $i<count($_code); $i++){
            $_code[$i] = trim($_code[$i]);
            if($this->debug)
                echo $_code[$i]. ' - ' . $i. '/'. count($_code)." - ASCII = ".ord($_code[$i])."\n";            
            $token = [];            
            if($_code[$i] === ' ' || ord($_code[$i]) == 0)
                continue;
            elseif (in_array($_code[$i], $this->symbols)) {
                $token['sym'] = 'sym';                
                if(isset($_code[$i+1]) && $_code[$i+1] == '=')
                    $token['value'] = $_code[$i].'=';        
                elseif($_code[$i] == '+' && $_code[$i+1] == '+')
                    $token['value'] = '++';
                elseif($_code[$i] == '-' && $_code[$i+1] == '-')
                    $token['value'] = '--';
                else
                    $token['value'] = $_code[$i];
                $tokens[] = $token;
            }
            elseif (is_numeric($_code[$i])){
                $intval = 0;
                $floatval = 0;         
                while(is_numeric($_code[$i])){                    
                    $intval = $intval * 10 + (int)$_code[$i];
                    $i++;                    
                }
                if($_code[$i] == '.'){
                    $i++;
                    while(is_numeric($_code[$i])){                    
                        $floatval = $floatval * 10 + (int)$_code[$i];
                        $i++;                    
                    }
                    if($floatval > 0)
                        $intval += $floatval * pow (0.1, strlen ((string)$floatval));
                }
                $token['sym'] = 'num';
                $token['value'] = $intval;
                $tokens[] = $token;
                $i--;
            }
            elseif($this->is_alpha($_code[$i])){
                $ident = '';                
                while($this->is_alpha($_code[$i])){
                    $ident .= $_code[$i];
                    $i++;
                }
                $i--;
                if(in_array($ident, $this->words)){
                    $token['sym'] = 'words';
                    $token['value'] = $ident;
                    $tokens[] = $token;
                }
                else{
                    $token['sym'] = 'var';
                    $token['value'] = $ident;
                    $tokens[] = $token;
                }
                
            }
            else
                echo "Unexpected symbol: '$_code[$i]'";
        }
        return $tokens;
    }
}

class Node {
    public $kind;
    public $value;
    public $op1;
    public $op2;
    public $op3;
    function Node($kind, $value = null, $op1 = null, $op2 = null, $op3 = null){
        $this->kind = $kind;
        $this->value = $value;
        $this->op1 = $op1;
        $this->op2 = $op2;
        $this->op3 = $op3;
    }
}

class Parser 
{
    public $operations = ['+', '-', '*', '/'];
    public $debug = false;
    public $tokens = [];
    public $token;
    public $i = 0;
    public function next_tok(){
        if($this->debug)
            echo "token_id = $this->i\n";
        if(isset($this->tokens[$this->i])){
            $this->token =  $this->tokens[$this->i];
            //print_r($this->token);
            $this->i++;
        }
        else
            $this->token = 'EOF';
    }
    public function term(){
        if($this->debug)
            echo "term \n";
        if($this->token['sym'] == 'var'){
            $n = new Node('var', $this->token['value']);
            $this->next_tok();
            return $n;
        }
        elseif($this->token['sym'] == 'num'){
            $n = new Node('num', $this->token['value']);
            if($this->debug)
                echo $this->token['value']."\n";
            $this->next_tok();
            if($this->debug)
                echo $this->token['value']."\n";
            return $n;
        }
        else
            return $this->paren_expr ();
    }
    public function summa(){
        if($this->debug)
            echo "summa \n";
        $n = $this->term();
        while(in_array($this->token['value'], $this->operations)){
            switch ($this->token['value']){
                case '+': $kind = 'add'; break;
                case '-': $kind = 'sub'; break;
                case '*': $kind = 'mul'; break;
                case '/': $kind = 'div'; break;
            }            
            $this->next_tok();
            $n = new Node($kind, $op1 = $n, $op2 = $this->term());
        }
        return $n;
    }
    public function test(){
        if($this->debug)
            echo "test \n";
        $n = $this->summa();
        switch($this->token['value']){
            case '<':
                $this->next_tok();
                $n = new Node('lt', $op1 = $n, $op2 = $this->summa());
                break;
            case '>':
                $this->next_tok();
                $n = new Node('mt', $op1 = $n, $op2 = $this->summa());
                break;
            case '==':
                $this->next_tok();
                $n = new Node('eq', $op1 = $n, $op2 = $this->summa());
                break;
            case '<=':
                $this->next_tok();
                $n = new Node('le', $op1 = $n, $op2 = $this->summa());
                break;
            case '>=':
                $this->next_tok();
                $n = new Node('me', $op1 = $n, $op2 = $this->summa());
                break;
            default: break;
                
        }
        return $n;
    }
    public function expr(){
        if($this->debug)
            echo "expr \n";
        if($this->token['sym'] != 'var')
            return $this->test();
        $n = $this->test();
        if($n->kind == 'var' && $this->token['value'] == '='){
            $this->next_tok();
            $n = new Node('set', $op1 = $n, $op2 = $this->expr());
        }
        return $n;
            
    }
    public function paren_for_expr(){
        if($this->debug)
            echo "paren_for_expr \n";
        if($this->token['value'] != '(')
            die("'(' expexted in paren for expr\n");
        $this->next_tok();
        if($this->token['value'] != ';'){
            $n1 = $this->expr();
            if($this->token['value'] != ';')
                die("';' expected in paren for expr\n");
        }
        else
            $n1 = null;
        $this->next_tok();
        if($this->token['value'] != ';'){
            $n2 = $this->test();
            if($this->token['value'] != ';')
                die("';' expected in paren for expr\n");
        }
        else
            $n2 = null;
        $this->next_tok();
        if($this->token['value'] != ')'){
            $n3 = $this->expr();
            if($this->token['value'] != ')')
                die("')' expected in paren for expr\n");
        }
        else
            $n3 = null;
        $this->next_tok();
        $n = new Node('forp', null, $op1 = $n1, $op2 = $n2, $op3 = $n3); 
        return $n;
    }
    public function paren_expr(){
        if($this->debug)
            echo "paren_expr \n";
        if($this->token['value'] != '(')
            die("'(' expexted. \n");
        $this->next_tok();
        $n = $this->expr();
        if($this->token['value'] != ')')
            die("')' expected. \n");
        $this->next_tok();
        return $n;        
    }
    public function statement(){
        if($this->debug)
            echo "statement \n";
        if($this->token['value'] == 'if'){
            $n = new Node('if1');
            $this->next_tok();
            $n->op1 = $this->paren_expr();
            $n->op2 = $this->statement();
            if($this->token['value'] == 'else'){
                $n->kind = 'if2';
                $this->next_tok();
                $n->op3 = $this->statement();
                
            }
        }
        elseif($this->token['value'] == 'for'){
            $n = new Node('for');
            $this->next_tok();
            $n->op1 = $this->paren_for_expr();
            $n->op2 = $this->statement();
        }
        elseif($this->token['value'] == 'while'){
            $n = new Node('while');
            $this->next_tok();
            $n->op1 = $this->paren_expr();
            $n->op2 = $this->statement();
        }
        elseif($this->token['value'] == 'do'){
            $n = new Node('do');
            $this->next_tok();
            $n->op1 = $this->statement();
            if($this->token['value'] != 'while')
                echo "'while' expected \n";
            $this->next_tok();
            $n->op2 = $this->paren_expr();
            if($this->token['value'] != ';')
                echo "';' expected \n";
        }
        elseif($this->token['value'] == ';'){
            $n = new Node('emp');
            $this->next_tok();
        }
        elseif($this->token['value'] == '{'){
            $n = new Node('emp');
            $this->next_tok();
            while($this->token['value'] != '}')
                $n = new Node('seq', $op1 = $n, $op2 = $this->statement ());
            $this->next_tok();
        }
        else{
            $n = new Node('expr', $op1 = $this->expr());
            if($this->token['value'] != ';'){
                //var_dump($n);
                die("';' expected on '{$this->token['value']}' token_num = {$this->i}\n");
            }
            $this->next_tok();
        }
        return $n;
            
    }

    public function parse(){
        $this->next_tok();
        $node = new Node('prog', $op1 = $this->statement());
        if($this->token != 'EOF')
            echo "Invalid statement sintax\n";
        return $node;
    }
    
    
}




//$l = new Lexer();
//$l->debug = true;
//$l->code = 'for(x=100;x<y; x=x+1)
//{s=s+x;}';
//$tokens = $l->getTokens();
////print_r($tokens);
//$p = new Parser();
//
//
//$p->debug = true;
//$p->tokens = $tokens;
//$tree = $p->parse();
//print_r($tree);
