<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Category;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
        </div> 
        <div class="col-md-6">
<?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::merge([null => 'Без родителя'], ArrayHelper::map(Category::find()->all(), 'id', 'name'))) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'info')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>
        </div>
    </div>
    <div class="form-group">
<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
