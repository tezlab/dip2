<?php
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>
        <p class="lead">Type your code.</p>
    </div>
    <div class="body-content">
        <div class="row">
             <?php ActiveForm::begin(['action' => '/index.php?r=site/run']);?>
                <div class="row">
                    <?= Html::textarea('code', '', ['rows' => 10, 'cols' => 100])?>
                </div>
                <div class="row">
                    <?= Html::submitInput('Отправить', ['class' => 'btn btn-success']) ?>
                </div>
            <?php ActiveForm::end();?>
        </div>

    </div>
</div>
