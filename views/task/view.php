<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Задачи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Решить', ['test', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?php if($model->checktype == 1) echo Html::a('Проверить', ['check', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            'begin:ntext',
            'body:ntext',
            'end:ntext',
            'result',
            //'extra:ntext',
            [
                'label' => 'Категория',                
                'attribute' => 'category.name',
            ]
            
        ],
    ]) ?>

</div>
<?php if (isset($res)): ?>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Результат проверки</h3>
        </div>
        <div class="panel-body">
            <table class="table">
                <tr>
                    <th>Входные данные</th>
                    <th>Выход</th>
                </tr>
                <?php foreach($res as $res2):?>
                <tr>
                    <td><?= $res2['input']?></td>
                    <td>
                        <?php 
                        if($res2['output']['compile_res'] == 0)
                            echo $res2['output']['result'];
                        else{
                            echo '<p><span class="label label-primary">Код возврата: '.$res2['output']['compile_res'].'</span></p>';
                            echo Html::tag('pre', $res2['code']);
                            echo '<pre>'.$res2['output']['compile'].'</pre>';
                        }
                        ?>
                    </td>
                    
                </tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>
<?php endif; ?>