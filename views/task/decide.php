<?php

use yii\helpers\Html;

if ($this->context->action->id == 'test-student-task') {
    $this->title = "Проверка задачи " . $model->name;
    $this->params['breadcrumbs'][] = ['label' => 'Студенты', 'url' => ['students']];
    $this->params['breadcrumbs'][] = ['label' => $user_task->user->username, 'url' => ['student', 'id' => $user_task->user_id]];
    $this->params['breadcrumbs'][] = $this->title;
} else {
    $this->title = "Решение задачи " . $model->name;
    $this->params['breadcrumbs'][] = ['label' => 'Задачи', 'url' => ['list']];
    $this->params['breadcrumbs'][] = $this->title;
}

$this->registerJsFile('/js/ace-builds/src-noconflict/ace.js');
$jsScript = 'var editor = ace.edit("editor");
    editor.setTheme("ace/theme/chrome");
    editor.setOption("minLines", 10);
    editor.setOption("maxLines", 50);   
    document.getElementById("editor").style.fontSize="14px";
    editor.getSession().setMode("ace/mode/c_cpp");
    editor.on("change", function(){
        $("#code").html(editor.getValue());
    });
    

';


$this->registerJs($jsScript);
?>
<div class="row">
    <div class="col-md-7">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Задача: <?= $model->name ?></h3>
            </div>
            <div class="panel-body">
                <p><?= $model->description ?></p>
                <div>
                    <p><span class="label label-default">Исходный код</span></p>            
                    <pre>
                        <?= Html::tag('div', Html::encode($model->begin)) ?>
                        <?= Html::tag('div', isset($_POST['code']) ? Html::encode($_POST['code']) : Html::encode($user_task->code), ['id' => 'editor']) ?>
                        <?= Html::tag('div', Html::encode($model->end)) ?>
                    </pre>
                    <?php yii\bootstrap\ActiveForm::begin(); ?>
                    <?= Html::textarea('code', isset($_POST['code']) ? Html::encode($_POST['code']) : Html::encode($user_task->code), ['rows' => 10, 'id' => 'code', 'class' => 'hidden']) ?>
                    <?= Html::submitButton('Запустить', ['class' => 'btn btn-success']) ?>
                    <?php yii\bootstrap\ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <?php if (isset($res['compile_res']) && $res['compile_res'] !== 0): ?>
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">Результат компиляции</h3>
                </div>
                <div class="panel-body">
                    <p><span class="label label-primary">Код возврата: <?= $res['compile_res'] ?></span></p>
                    <pre><?= $res['compile'] ?></pre>    
                </div>
            </div>
        <?php endif; ?>
        <?php if (isset($res['result'])): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Результат выполнения</h3>
                </div>
                <div class="panel-body">
                    <pre><?= $res['result'] ?></pre>
                </div>
            </div>
        <?php endif; ?>
        <?php if (isset($res2_all)): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Результат проверки</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <th>Входные данные</th>
                            <th>Ответ</th>
                            <th>Выход</th>
                        </tr>
                        <?php foreach ($res2_all as $res2): ?>
                            <tr>
                                <td><?= $res2['input'] ?></td>
                                <td><?= $res2['origin_out']['result'] ?></td>                    
                                <td>
                                    <?php
                                    if ($res2['output']['compile_res'] == 0)
                                        echo $res2['output']['result'];
                                    else {
                                        echo '<p><span class="label label-primary">Код возврата: ' . $res2['output']['compile_res'] . '</span></p>';
                                        echo Html::tag('pre', $res2['code']);
                                        echo '<pre>' . $res2['output']['compile'] . '</pre>';
                                    }
                                    ?>
                                    <?= $res2['decided'] ? '<span class="pull-right glyphicon glyphicon-ok"></span>' : '<span class="pull-right glyphicon glyphicon-remove"></span>' ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-md-5">        
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Справочная информация</h3>
            </div>
            <div class="panel-body">
                <?= $model->category->info ?>
            </div>
        </div>
    </div>
</div>