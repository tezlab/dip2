<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('/js/ace-builds/src-noconflict/ace.js');

$jsScript2 = 'var editor1 = ace.edit("editor1");
    editor1.setTheme("ace/theme/chrome");
    editor1.setOption("minLines", 10);
    editor1.setOption("maxLines", 50);
    editor1.setAutoScrollEditorIntoView(true);   
    document.getElementById("editor1").style.fontSize="14px";
    editor1.getSession().setMode("ace/mode/c_cpp");
    var editor2 = ace.edit("editor2");
    editor2.setTheme("ace/theme/chrome");
    editor2.setOption("minLines", 10);
    editor2.setOption("maxLines", 50);
    editor2.setAutoScrollEditorIntoView(true);   
    document.getElementById("editor2").style.fontSize="14px";
    editor2.getSession().setMode("ace/mode/c_cpp");
    var editor3 = ace.edit("editor3");
    editor3.setTheme("ace/theme/chrome");
    editor3.setOption("minLines", 10);
    editor3.setOption("maxLines", 50);
    editor3.setAutoScrollEditorIntoView(true);   
    document.getElementById("editor3").style.fontSize="14px";
    editor3.getSession().setMode("ace/mode/c_cpp");
    
    editor1.on("change", function(){
        $("#code1").html(editor1.getValue());
    });
    editor2.on("change", function(){
        $("#code2").html(editor2.getValue());
    });
    editor3.on("change", function(){
        $("#code3").html(editor3.getValue());
    });
';

$jsScript = " 
    $('body').on('click', '.add-var', function(){
        $('.vars').append($('.default-var').html());
    });
    $('body').on('click', '.del-var', function(){
        $(this).parent().parent().remove();
    });
    $('body').on('change', '#task-checktype', function(){
        if($(this).val() == 1)
            $('.vars-check').removeClass('hidden');
        else
            $('.vars-check').addClass('hidden');
    });
    ";
$this->registerJs($jsScript);
$this->registerJs($jsScript2);
?>

<div class="task-form">
    <div class="hidden default-var">
        <div class="row">
            <div class="col-md-3"><?= Html::dropDownList('vartype[]', null, [1 => 'int', 2 => 'float'], ['class' => 'form-control']) ?></div>
            <div class="col-md-3"><?= Html::input('text', 'varname[]', '', ['class' => 'form-control']) ?></div>
            <div class="col-md-5"><?= Html::input('text', 'varparam[]', '', ['class' => 'form-control']) ?></div>
            <div class="col-md-1"><div class="btn btn-danger btn-xs pull-left del-var"><span class="glyphicon glyphicon-remove"></span></div></div>
        </div>
    </div>
<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
            <?= $form->field($model, 'category_id')->dropDownList(yii\helpers\ArrayHelper::map(Category::find()->all(), 'id', 'name')) ?>   
            <?= $form->field($model, 'begin')->textarea(['rows' => 5, 'id'=>'code1', 'class' => 'hidden']) ?>
            <?= Html::tag('div', Html::encode($model->begin), ['id' => 'editor1', 'class' => 'form-control'])?>
            <?= $form->field($model, 'body')->textarea(['rows' => 5, 'id' => 'code2', 'class' => 'hidden']) ?>
            <?= Html::tag('div', Html::encode($model->body), ['id' => 'editor2', 'class' => 'form-control'])?>
            <?= $form->field($model, 'end')->textarea(['rows' => 5, 'id' => 'code3', 'class' => 'hidden']) ?> 
            <?= Html::tag('div', Html::encode($model->end), ['id' => 'editor3', 'class' => 'form-control'])?>
            
            
            
                       
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'description')->textarea(['rows' => 5]) ?>
            <?= $form->field($model, 'checktype')->dropDownList($model->getChecktypes()) ?>  
            <div class="panel panel-primary vars-check <?= $model->checktype==1?'':'hidden'?>">
                <div class="panel-body">
                    <h4>Добавьте переменных для перебора.</h4>
                    <h5 class="subheader"></h5>
                    <div class="vars">
                        <div class="row">
                            <div class="col-md-3"><?= Html::label('Тип', ['class' => 'control-label'])?></div>
                            <div class="col-md-3"><?= Html::label('Имя', ['class' => 'control-label'])?></div>                            
                            <div class="col-md-6"><?= Html::label('Параметры(min; max)', ['class' => 'control-label'])?></div>
                        </div>
                        <?php $vars = json_decode($model->extra, true); if($model->isNewRecord || empty($vars)):?>
                        <div class="row">
                            <div class="col-md-3"><?= Html::dropDownList('vartype[]', null, [1 => 'int', 2 => 'float'], ['class' => 'form-control'])?></div>
                            <div class="col-md-3"><?= Html::input('text', 'varname[]','', ['class' => 'form-control'])?></div>
                            <div class="col-md-5"><?= Html::input('text', 'varparam[]','', ['class' => 'form-control'])?></div>
                            <div class="col-md-1"><div class="btn btn-danger btn-xs pull-left del-var"><span class="glyphicon glyphicon-remove"></span></div></div>
                        </div>
                        <?php else: ?>
                        <?php for($i=0; $i<count($vars['varname']); $i++):?>
                        <div class="row">
                            <div class="col-md-3"><?= Html::dropDownList("vartype[$i]", $vars['vartype'][$i], [1 => 'int', 2 => 'float'], ['class' => 'form-control'])?></div>
                            <div class="col-md-3"><?= Html::input('text', "varname[$i]",$vars['varname'][$i], ['class' => 'form-control'])?></div>
                            <div class="col-md-5"><?= Html::input('text', "varparam[$i]",$vars['varparam'][$i], ['class' => 'form-control'])?></div>
                            <div class="col-md-1"><div class="btn btn-danger btn-xs pull-left del-var"><span class="glyphicon glyphicon-remove"></span></div></div>
                        </div>
                        <?php endfor;?>
                        
                        <?php endif;?>
                    </div>
                    <p>
                        <div class="btn btn-primary add-var"><span class="glyphicon glyphicon-plus"></span></div>
                    </p>
                    
                </div>
              </div>
        </div>
    </div>
    

    <p>
    <div class="form-group">
<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить редактирование', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</p>
<?php ActiveForm::end(); ?>

</div>
