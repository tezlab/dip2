<?php

use webvimark\modules\UserManagement\components\GhostHtml;
use webvimark\modules\UserManagement\models\rbacDB\Role;
use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use webvimark\extensions\GridBulkActions\GridBulkActions;
use webvimark\extensions\GridPageSize\GridPageSize;
use yii\grid\GridView;
use yii\widgets\ListView;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var webvimark\modules\UserManagement\models\search\UserSearch $searchModel
 */

$this->title = 'Студенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

	<h2 class="lte-hide-title"><?= $this->title ?></h2>

	<div class="panel panel-default">
		<div class="panel-body">


			<?php Pjax::begin([
				'id'=>'user-grid-pjax',
			]) ?>
                        <table class="table">
                            <tr>
                                <th>Логин студента</th>
                                <th>Количество задач</th>
                                <th>Количество правильных решений</th>
                            </tr>
			<?= ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => '_student',
                        ])?>
                        </table>
			<?php Pjax::end() ?>

		</div>
	</div>
</div>