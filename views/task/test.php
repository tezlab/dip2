<?php 
use yii\helpers\Html;
$this->title = "Проверка задачи ".$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Задачи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Решение';

?>
<p>
    <?= Html::a('Создать задачу', ['create'], ['class' => 'btn btn-success']) ?>  
    <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
</p>

<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Задача: <?=$model->name?></h3>
  </div>
  <div class="panel-body">
      <p><?=$model->description?></p>
      <?php if($model->result):?>
      <h4>
          Ответ: <mark><?= $model->result?></mark>
      </h4>
      <?php endif;?>
      <div>
          <p><span class="label label-default">Исходный код</span></p>
          <pre><?= Html::encode($model->begin.$model->body.$model->end)?></pre>
      </div>
  </div>
</div>
<?php if($compile_res !== 0):?>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Результат компиляции</h3>
  </div>
  <div class="panel-body">
      <p><span class="label label-primary">Код возврата: <?=$compile_res?></span></p>
      <pre><?= $compile ?></pre>    
  </div>
</div>
<?php endif;?>
<?php if (!empty($result)): ?>
<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">Результат выполнения</h3>
  </div>
  <div class="panel-body">
      <pre><?= $result ?></pre>
      <?= Html::a('Сохранить ответ', ['task/saveresult', 'id' => $model->id], ['class' => 'btn btn-success'])?>
  </div>
</div>
<?php endif; ?>