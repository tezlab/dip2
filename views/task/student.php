<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use webvimark\modules\UserManagement\models\User;

$this->title = "Студент " . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Студенты', 'url' => ['students']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="panel panel-info">
    <div class="panel-heading">Информация о студенте</div>
    <div class="panel-body">    
        <?= DetailView::widget([
            'model'      => $model,
            'attributes' => [
                    'id',
                    [
                            'attribute'=>'status',
                            'value'=>User::getStatusValue($model->status),
                    ],
                    'username',
                    [
                            'attribute'=>'email',
                            'value'=>$model->email,
                            'format'=>'email',
                            'visible'=>User::hasPermission('viewUserEmail'),
                    ],
                    [
                            'attribute'=>'email_confirmed',
                            'value'=>$model->email_confirmed,
                            'format'=>'boolean',
                            'visible'=>User::hasPermission('viewUserEmail'),
                    ],                    
                    'created_at:datetime',
                    'updated_at:datetime',
            ],
    ]) ?>
    </div>
    
</div>
<div class="panel panel-primary">
    <div class="panel-heading">Задачи</div>
    <div class="panel-body">  
        <table class="table table-striped">
            <tr>
                <th>Название задачи</th>
                <th>Категория</th>
                <th>Описание</th>
                <th>Статус</th>
                <th></th>
            </tr>
            <?php foreach($tasks as $key => $item):?>
            <tr <?= $item->is_correct == '0' ? 'class="bg-warning"':''?>>
                <td><?= Html::a($item->task->name, ['view', 'id'=>$item->task_id])?></td>
                <td><?= $item->task->category->name?></td>
                <td><?= Html::encode($item->task->description)?></td>
                <td><?= $item->is_correct == '1' ? 'Задача решена правильно.': '<label class="text-danger">Задача решена не правильно.</label>'?></td>
                <td>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" aria-expanded="true"  data-target="#collapse<?= $key?>" >
                        Посмотреть решение
                      </button>
                </td>
            </tr>
            <tr class="collapse" id="collapse<?= $key?>">
                <td colspan="4">
                    <pre>
                        <?= Html::encode($item->task->begin. $item->code. $item->task->end)?>
                    </pre>
                </td>
                <td>
                    <?= Html::a('Запустить', ['test-student-task', 'id' => $item->task_id, 'student_id' => $item->user_id], ['class' => 'btn btn-warning'])?>
                </td>
            </tr>
            <?php endforeach;?>
        </table>
    </div>
</div>