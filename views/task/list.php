<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Category;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список задач | Ученик';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            [
                'label' => 'Категория',
                'value' => 'category.name',
                'filter' => Html::activeDropDownList($searchModel, 'category_id', ArrayHelper::merge([null => 'Без категории'], ArrayHelper::map(Category::find()->all(), 'id', 'name')), ['class'=>'form-control']),                
            ],
            'description:ntext',
            [
                'label' => 'Статус',
                'format' => 'raw',
                'value' => function($model){
                    $task = $model->getUserTask();
                    if(isset($task))
                        if($task->is_correct=='1')
                            return 'Задача решена правильно.';
                        else
                            return '<label class="text-danger">Задача решена не правильно.</label>';
                    else
                        return '';
                            
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'decide' => function($url, $model){
                        $task = $model->getUserTask();
                    if(isset($task))
                        if($task->is_correct=='1')
                            return Html::a('Просмотреть', Yii::$app->getUrlManager()->createUrl(['task/decide', 'id'=>$model->id]), ['class' => 'btn btn-sm btn-primary']);
                        else
                            return Html::a('Просмотреть', Yii::$app->getUrlManager()->createUrl(['task/decide', 'id'=>$model->id]), ['class' => 'btn btn-sm btn-danger']);
                    else
                        return Html::a('Решить', Yii::$app->getUrlManager()->createUrl(['task/decide', 'id'=>$model->id]), ['class' => 'btn btn-sm btn-success']);
                    }
                ],
                'template' => '{decide}',
            ],
        ],
    ]); ?>

</div>
