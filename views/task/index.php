<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Category;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список задач | Преподаватель';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать задачу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            [
                'label' => 'Категория',
                'value' => 'category.name',
                'filter' => Html::activeDropDownList($searchModel, 'category_id', ArrayHelper::map(Category::find()->all(), 'id', 'name'), ['prompt' => 'Без категории', 'class' => 'form-control']),                
            ],
            'description:ntext',
            //'begin:ntext',
            //'body:ntext',
            // 'end:ntext',
            // 'extra:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{test}{view}{update}{delete}',
                'buttons' => [
                    'test' => function($url, $model){
                        return Html::a('Проверить', Yii::$app->getUrlManager()->createUrl(['task/test', 'id'=>$model->id]), ['class' => 'btn btn-xs btn-success']);

                    },
                    'tree' => function($url, $model){
                        return Html::a('Дерево', Yii::$app->getUrlManager()->createUrl(['task/tree', 'id'=>$model->id]), ['class' => 'btn btn-xs btn-success']);

                    }
                ],
            ],
        ],
    ]); ?>

</div>
