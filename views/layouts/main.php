<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

use webvimark\modules\UserManagement\components\GhostMenu;
use webvimark\modules\UserManagement\UserManagementModule;
use webvimark\modules\UserManagement\models\User;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
        $main_menu = [];
        if(Yii::$app->user->isGuest){
            $main_menu = [
                ['label' => 'Войти', 'url' => ['/login']],
                ['label' => 'Регистрация', 'url' => ['/registration']] 
            ];
        }
        elseif(User::hasRole(['student'], false)){
            $main_menu = [
                ['label' => 'Задачи (У)', 'url' => ['/task/list']],
                ['label'=>'Сменить пароль', 'url'=>['/user-management/auth/change-own-password']],
                ['label' => 'Выйти (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/logout'],
                            'linkOptions' => ['data-method' => 'post']],
                
            ];
        }
        elseif(User::hasRole(['Admin'])){
            $main_menu = [
//                [
//                    'label' => 'Управление пользователями',
//                    'items'=>UserManagementModule::menuItems(),
//                ],
                ['label' => 'Категории', 'url' => ['/category/index']],
                ['label' => 'Задачи (П)', 'url' => ['/task/index']],
                ['label' => 'Студенты', 'url' => ['/task/students']],
                ['label' => 'Сменить пароль', 'url'=>['/user-management/auth/change-own-password']],
                ['label' => 'Выйти (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/logout'],
                            'linkOptions' => ['data-method' => 'post']],                
            ];
        }
        
            NavBar::begin([
                'brandLabel' => 'Dip 2015',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                
                'items' => $main_menu,
            ]);
            NavBar::end();
        ?>
        <div class="container">

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'homeLink' => ['label' => 'Главная', 'url' => '/'],
            ]) ?>
            
            
                <?php foreach (Yii::$app->session->getAllFlashes() as $key => $message):?>
                    <div class="row">
                        <div class="col-lg-12">        
                            <div class="alert alert-<?= $key ?>" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>        
                                <?= $message ?>                                
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; Dip <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
